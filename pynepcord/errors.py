"""
MIT License

Copyright (c) 2023 Starrysparklez, MadCat995
Copyright (c) 2020 Starrysparklez

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


class NepuError(BaseException):
    """Base exception class that represents pynepcord errors."""


class InvalidUserID(NepuError):
    """Exception that raises when given invalid User ID."""


class InvalidCategory(NepuError):
    """Exception that raises when given invalid image category."""


class UserNotFound(NepuError):
    """Exception that raises when given user wasn't found."""


class Unauthorized(NepuError):
    """Exception that raises when given API key is incorrect."""


class Forbidden(NepuError):
    """Exception that raises when you don't have permission to use method."""


class Unprocessable(NepuError):
    """Exception that raises when the user/guild/bot already in the database"""
